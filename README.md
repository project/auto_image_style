# Auto image style (D8 port)

An initial Drupal 8 port of https://www.drupal.org/project/auto_image_style.

## Contributing
Now the development is fully on https://git.drupalcode.org/project/auto_image_style

The list of issues and the list of contributors is located on the original link of the project https://www.drupal.org/project/auto_image_style
