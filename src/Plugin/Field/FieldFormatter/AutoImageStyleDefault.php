<?php

namespace Drupal\auto_image_style\Plugin\Field\FieldFormatter;

use Drupal\Core\Cache\Cache;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\image\Plugin\Field\FieldFormatter\ImageFormatter;

/**
 * Image auto orientation.
 *
 * @FieldFormatter(
 *  id = "auto_image_style_default",
 *  label = @Translation("Image auto orientation"),
 *  description = @Translation("Display image fields as portrait or landscape style"),
 *  field_types = {
 *    "image",
 *  }
 * )
 */
class AutoImageStyleDefault extends ImageFormatter {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'image_style_portrait' => '',
      'image_style_landscape' => '',
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $image_styles = image_style_options(FALSE);
    $element = parent::settingsForm($form, $form_state);
    unset($element['image_style']);
    $element['image_style_portrait'] = [
      '#type' => 'select',
      '#title' => $this->t('Portrait image style'),
      '#options' => $image_styles,
      '#empty_option' => $this->t('None (original image)'),
      '#default_value' => $this->getSetting('image_style_portrait'),
      '#description' => $this->t('Select the image style for portrait images'),
    ];
    $element['image_style_landscape'] = [
      '#type' => 'select',
      '#title' => $this->t('Landscape image style'),
      '#options' => $image_styles,
      '#empty_option' => $this->t('None (original image)'),
      '#default_value' => $this->getSetting('image_style_landscape'),
      '#description' => $this->t('Select the image style for landscape images'),
    ];
    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];

    $image_styles = image_style_options(FALSE);
    // Unset possible 'No defined styles' option.
    unset($image_styles['']);
    // Styles could be lost because of enabled/disabled modules that defines
    // their styles in code.
    $image_style_portrait_setting = $this->getSetting('image_style_portrait');
    if (isset($image_styles[$image_style_portrait_setting])) {
      $summary[] = $this->t('Portrait image style: @style', ['@style' => $image_styles[$image_style_portrait_setting]]);
    }
    else {
      $summary[] = $this->t('Portrait image style: Original image');
    }

    $image_style_landscape_setting = $this->getSetting('image_style_landscape');
    if (isset($image_styles[$image_style_landscape_setting])) {
      $summary[] = $this->t('Landscape image style: @style', ['@style' => $image_styles[$image_style_landscape_setting]]);
    }
    else {
      $summary[] = $this->t('Landscape image style: Original image');
    }

    $link_types = [
      'content' => $this->t('Linked to content'),
      'file' => $this->t('Linked to file'),
    ];
    // Display this setting only if image is linked.
    if (isset($link_types[$this->getSetting('image_link')])) {
      $summary[] = $link_types[$this->getSetting('image_link')];
    }

    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];
    $files = $this->getEntitiesToView($items, $langcode);

    // Early opt-out if the field is empty.
    if (empty($files)) {
      return $elements;
    }

    // Currently no link handling.
    $url = NULL;

    $image_link_setting = $this->getSetting('image_link');
    // Check if the formatter involves a link.
    if ($image_link_setting == 'content') {
      $entity = $items->getEntity();
      if (!$entity->isNew()) {
        $url = $entity->toUrl();
      }
    }
    elseif ($image_link_setting == 'file') {
      $link_file = TRUE;
    }

    $image_style_landscape_setting = $this->getSetting('image_style_landscape');
    $image_style_portrait_setting = $this->getSetting('image_style_portrait');

    // Collect cache tags to be added for each item in the field.
    $cache_tags = [];
    if (!empty($image_style_landscape_setting)) {
      $image_style = $this->imageStyleStorage->load($image_style_landscape_setting);
      $cache_tags_landscape = $image_style->getCacheTags();
      $cache_tags = Cache::mergeTags($cache_tags, $cache_tags_landscape);
    }
    if (!empty($image_style_portrait_setting)) {
      $image_style = $this->imageStyleStorage->load($image_style_portrait_setting);
      $cache_tags_portrait = $image_style->getCacheTags();
      $cache_tags = Cache::mergeTags($cache_tags, $cache_tags_portrait);
    }

    foreach ($files as $delta => $file) {
      if (isset($link_file)) {
        $image_uri = $file->getFileUri();
        $url = $this->fileUrlGenerator->generate($image_uri);
      }

      $cache_tags = Cache::mergeTags($cache_tags, $file->getCacheTags());

      // Extract field item attributes for the theme function, and unset them
      // from the $item so that the field template does not re-render them.
      $item = $file->_referringItem;

      $image_style = $image_style_portrait_setting;
      if ($item->height < $item->width) {
        $image_style = $image_style_landscape_setting;
      }

      $item_attributes = $item->_attributes;
      unset($item->_attributes);

      $elements[$delta] = [
        '#theme' => 'image_formatter',
        '#item' => $item,
        '#item_attributes' => $item_attributes,
        '#image_style' => $image_style,
        '#url' => $url,
        '#cache' => [
          'tags' => $cache_tags,
        ],
      ];
    }

    return $elements;
  }

}
